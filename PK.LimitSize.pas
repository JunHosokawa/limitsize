﻿(*
 * Window size limiter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2017, 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.LimitSize;
 *
 *   type
 *     TForm1 = class(TForm)
 *       procedure FormCreate(Sender: TObject);
 *     private
 *       FLimitSize: TLimitSize;
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     FLimitSize := TLimitSize.Create(Self);
 *     FLimitSize.SetLimit(180, 160, 1024, 768);
 *   end;
 *
 * 2017/11/17 Version 1.0.3
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.LimitSize;

interface

uses
  System.Classes
  , FMX.Types
  ;

type
  ILimitSize = interface
    ['{113C3A3C-5BED-4C39-BB68-225D451F1023}']
    function SetLimit(
      const AHandle: TWindowHandle;
      const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
  end;

  ILimitSizeFactory = interface(IInterface)
    ['{0926ABAA-7324-47FD-A789-C6457613F37D}']
    function CreateLimitSize: ILimitSize;
  end;

  TLimitSizeFactory = class(TInterfacedObject, ILimitSizeFactory)
  public
    function CreateLimitSize: ILimitSize; virtual; abstract;
  end;

  [ComponentPlatformsAttribute(pidWin32 or pidWin64 or pidOSX32)]
  TLimitSize = class(TFmxObject)
  private var
    FLimitSize: ILimitSize;
    FMinHeight: Integer;
    FMaxWidth: Integer;
    FMinWidth: Integer;
    FMaxHeight: Integer;
  private
    procedure SetMaxHeight(const Value: Integer);
    procedure SetMaxWidth(const Value: Integer);
    procedure SetMinHeight(const Value: Integer);
    procedure SetMinWidth(const Value: Integer);
  public
    constructor Create(AOwner: TComponent); override;
    function SetLimit(
      const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
  published
    property MinWidth: Integer read FMinWidth write SetMinWidth;
    property MinHeight: Integer read FMinHeight write SetMinHeight;
    property MaxWidth: Integer read FMaxWidth write SetMaxWidth;
    property MaxHeight: Integer read FMaxHeight write SetMaxHeight;
  end;

procedure Register;

implementation

uses
  System.SysUtils
  , FMX.Forms, FMX.Platform
  {$IFDEF MSWINDOWS}
    , PK.LimitSize.Win
  {$ENDIF}
  {$IFDEF MACOS}
    , PK.LimitSize.Mac
  {$ENDIF}
  ;

procedure Register;
begin
  RegisterComponents('PK', [TLimitSize]);
end;

{ TLimitSize }

constructor TLimitSize.Create(AOwner: TComponent);
var
  LimitSizeFactory: ILimitSizeFactory;
begin
  inherited;

  if (csDesigning in ComponentState) then
    Exit;

  if
    (
      TPlatformServices.Current.SupportsPlatformService(
        ILimitSizeFactory,
        IInterface(LimitSizeFactory)
      )
    )
  then
    FLimitSize := LimitSizeFactory.CreateLimitSize;
end;

function TLimitSize.SetLimit(
  const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
var
  Form: TCommonCustomForm;
  W, H: Integer;
begin
  Result := False;

  FMinWidth := AMinWidth;
  FMinHeight := AMinHeight;
  FMaxWidth := AMaxWidth;
  FMaxHeight := AMaxHeight;

  if (FLimitSize <> nil) and (Owner is TCommonCustomForm) then
  begin
    Form := (Owner as TCommonCustomForm);

    Result :=
      FLimitSize.SetLimit(
        Form.Handle,
        FMinWidth, FMinHeight,
        FMaxWidth, FMaxHeight);

    W := Form.Width;
    H := Form.Height;

    if (W < FMinWidth) and (FMinWidth > 0) then
      W := FMinWidth;

    if (W > FMaxWidth) and (FMaxWidth > 0) then
      W := FMaxWidth;

    if (H < FMinHeight) and (FMinHeight > 0) then
      H := FMinHeight;

    if (H > FMaxHeight) and (FMaxHeight > 0) then
      H := FMaxHeight;

    Form.SetBounds(Form.Left, Form.Top, W, H);
  end;
end;

procedure TLimitSize.SetMaxHeight(const Value: Integer);
begin
  if FMaxHeight <> Value then
    SetLimit(FMinWidth, FMinHeight, FMaxWidth, Value);
end;

procedure TLimitSize.SetMaxWidth(const Value: Integer);
begin
  if FMaxWidth <> Value then
    SetLimit(FMinWidth, FMinHeight, Value, FMaxHeight);
end;

procedure TLimitSize.SetMinHeight(const Value: Integer);
begin
  if FMinHeight <> Value then
    SetLimit(FMinWidth, Value, FMaxWidth, FMaxHeight);
end;

procedure TLimitSize.SetMinWidth(const Value: Integer);
begin
  if FMinWidth <> Value then
    SetLimit(Value, FMinHeight, FMaxWidth, FMaxHeight);
end;

end.
