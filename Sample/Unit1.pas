unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.StdCtrls, FMX.Controls.Presentation, PK.LimitSize;

type
  TfrmSample = class(TForm)
    styleCopper: TStyleBook;
    styleAmakrits: TStyleBook;
    grpbxStyles: TGroupBox;
    rdoStyleNone: TRadioButton;
    rdoStyleNormal: TRadioButton;
    rdoStylePremium: TRadioButton;
    btnLimit: TButton;
    procedure ChangeStyle(Sender: TObject);
    procedure btnLimitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FLimitSize: TLimitSize;
    procedure SetLimitText;
  public
  end;

var
  frmSample: TfrmSample;

implementation

{$R *.fmx}

procedure TfrmSample.btnLimitClick(Sender: TObject);

  function Scaled(const Size: Integer; const IsVert: Boolean): Integer;
  begin
    {$IFDEF MSWINDOWS}
    Result := Trunc(Size * Handle.Scale);
    {$ENDIF}

    {$IFDEF MACOS}
    // Border の幅やタイトルバーの高さ等。本当は計算から求める
    Result := Size - 16 - Ord(IsVert) * 24;
    {$ENDIF}
  end;

begin
  if (FLimitSize = nil) then
  begin
    FLimitSize := TLimitSize.Create(Self);
    FLimitSize.SetLimit(
      Scaled(218, False),
      Scaled(190, True),
      Scaled(480, False),
      Scaled(320, True));
  end
  else
    FreeAndNil(FLimitSize);

  SetLimitText;
end;

procedure TfrmSample.ChangeStyle(Sender: TObject);
begin
  TThread.ForceQueue(
    TThread.Current,
    procedure
    begin
      if (rdoStyleNone.IsChecked) then
        StyleBook := nil;

      if (rdoStyleNormal.IsChecked) then
        StyleBook := styleAmakrits;

      if (rdoStylePremium.IsChecked) then
        StyleBook := styleCopper;

      frmSample.ResizeHandle;
    end
  );
end;

procedure TfrmSample.FormCreate(Sender: TObject);
begin
  SetLimitText;
end;

procedure TfrmSample.SetLimitText;
const
  LIMIT_TEXT: array [Boolean] of String = ('Unlimit', 'Limit');
begin
  btnLimit.Text := LIMIT_TEXT[FLimitSize = nil];
end;

end.
