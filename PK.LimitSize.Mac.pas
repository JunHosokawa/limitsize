﻿(*
 * Window size limiter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2017, 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.LimitSize;
 *
 *   type
 *     TForm1 = class(TForm)
 *       procedure FormCreate(Sender: TObject);
 *     private
 *       FLimitSize: TLimitSize;
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     FLimitSize := TLimitSize.Create(Self);
 *     FLimitSize.SetLimit(180, 160, 1024, 768);
 *   end;
 *
 * 2018/04/11 Version 1.0.3  Check valid wnd at destory.
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.LimitSize.Mac;

{$IFNDEF MACOS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

implementation

uses
  System.Classes
  , PK.LimitSize
  , Macapi.CocoaTypes, Macapi.AppKit
  , FMX.Types, FMX.Platform, FMX.Platform.Mac, FMX.Forms
  ;

type
  TLimitSizeMac = class(TInterfacedObject, ILimitSize)
  private
    FWnd: NSWindow;
    FWndRef: Pointer;
    FOrgMinSize: NSSize;
    FOrgMaxSize: NSSize;
  public
    function SetLimit(
      const AHandle: TWindowHandle;
      const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
    destructor Destroy; override;
  end;

  TLimitSizeFactoryMac = class(TLimitSizeFactory)
  private class var
    SSelf: TLimitSizeFactoryMac;
  public
    function CreateLimitSize: ILimitSize; override;
  end;

procedure RegisterLimitSizeMac;
begin
  TLimitSizeFactoryMac.SSelf := TLimitSizeFactoryMac.Create;
  TPlatformServices.Current.AddPlatformService(
    ILimitSizeFactory,
    TLimitSizeFactoryMac.SSelf as ILimitSizeFactory);
end;

{ TLimitSizeMac }

destructor TLimitSizeMac.Destroy;
var
  i: Integer;
  Form: TCommonCustomForm;
  FormWnd: NSWindow;
  Valid: Boolean;
begin
  Valid := False;

  for i := 0 to Screen.FormCount - 1 do
  begin
    FormWnd := WindowHandleToPlatform(Screen.Forms[i].Handle).Wnd;
    if (FormWnd <> nil) and (FormWnd.windowRef = FWndRef) then
    begin
      Valid := True;
      Break;
    end;
  end;

  if Valid then
  begin
    FWnd.setContentMinSize(FOrgMinSize);
    FWnd.setContentMaxSize(FOrgMaxSize);
  end;

  inherited;
end;

function TLimitSizeMac.SetLimit(
  const AHandle: TWindowHandle;
  const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
var
  Size: NSSize;
begin
  FWnd := WindowHandleToPlatform(AHandle).Wnd;
  FWndRef := FWnd.windowRef;

  // 最小値の設定
  Size.width := FWnd.contentMinSize.width;
  Size.height := FWnd.contentMinSize.height;
  FOrgMinSize := Size;

  if (AMinWidth > 0) then
    Size.width := AMinWidth;

  if (AMinHeight > 0) then
    Size.height := AMinHeight;

  FWnd.setContentMinSize(Size);

  // 最大値の設定
  Size.width := FWnd.contentMaxSize.width;
  Size.height := FWnd.contentMaxSize.height;
  FOrgMaxSize := Size;

  if (AMaxWidth > 0) then
    Size.width := AMaxWidth;

  if (AMaxHeight > 0) then
    Size.height := AMaxHeight;

  FWnd.setContentMaxSize(Size);

  Result := True;
end;

{ TLimitSizeFactoryMac }

function TLimitSizeFactoryMac.CreateLimitSize: ILimitSize;
begin
  Result := TLimitSizeMac.Create;
end;

initialization
  RegisterLimitSizeMac;

end.
